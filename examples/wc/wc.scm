(use-modules (silex silex))
(lex "wc.l" "wc.l.scm")
(lex-tables "wc.l" "wc-table" "wc.t.scm")
(load "wc.l.scm")
(load "wc.t.scm")
(define (wc str)
  (lexer-init 'string str)
  (lexer))
