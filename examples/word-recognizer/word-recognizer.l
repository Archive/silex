;; this sample demonstrates (very) simple recognition:
;; a verb/not a verb.
verb "is"|"am"|"are"|"were"|"was"|"be"|"being"|"been"|"do"|"does"|"did"|"will"|"would"|"could" 
word [A-Za-z]+

%%
[\t ]+ (yycontinue)
{verb} (for-each display `(,yytext ": is a verb\n"))(yycontinue)
{word} (for-each display `(,yytext ": is not a verb\n"))(yycontinue)














