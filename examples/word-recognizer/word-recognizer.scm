(use-modules (silex silex))

(lex "word-recognizer.l" "word.l.scm")
(lex-tables "word-recognizer.l" "word-table" "word.t.scm")

(load "word.l.scm")
(load "word.t.scm")

(define (lex-string str)
  (lexer-init 'string str)
  (lexer))
